from typing import Dict, List

import commentjson
import numpy as np
import pandas as pd

from claz import util, const
from claz.airport import Airport
from claz.filteredLinks import FilteredLinks
from claz.const import Con, Var, Edg, VAL, DESCRIPTION
from claz.station import Station
from claz.util import Karte, LinkData, get_pas_conn
from fly import Fly
from rail import Rail


class Kiss:
    def __init__(self, fly: Fly, rail: Rail):
        self.fly = fly
        self.rail = rail
        self.station_to_airports, self.airport_to_station = self._map_station_airport()
        self.add_distance_data_to_rail_graph()
        self.rail.find_detours()

        self.flt_links = FilteredLinks(self.calc_categorized_connections())
        self.flt_links.print((None, Con.EEA, Con.CORE))
        # self.flt_links.generate_table((None, Con.EEA, Con.CORE))
        self.links: pd.DataFrame = self.flt_links[Con.VALID].drop(columns=["orig_err", "dest_err"])

    def _map_station_airport(self):
        airports_by_simple_name: Dict[str, List[Airport]] = {}
        for airport in self.fly.airports.values():
            if airport.simple_name not in airports_by_simple_name:
                airports_by_simple_name[airport.simple_name] = []
            airports_by_simple_name[airport.simple_name].append(airport)

        # TODO: bring this together into one file
        with open(const.RAIL_TO_FLY_MAP_CORRECTION_DATA, "r") as f:
            mapping_corrections: Dict[str, List[str]] = commentjson.load(f)

        station_to_airports: Dict[Station, List[Airport]] = {}
        for station in self.rail.station_list:
            if station.code in mapping_corrections:
                airport_codes = [airport_code for airport_code
                                 in mapping_corrections[station.code]]
                station_to_airports[station] = []
                for airport_code in airport_codes:
                    try:
                        station_to_airports[station].append(self.fly.airports[airport_code])
                    except KeyError:
                        continue  # this Airport was not important enough to make the cut.
            elif station.simple_name in airports_by_simple_name:
                station_to_airports[station] = airports_by_simple_name[station.simple_name]
            else:
                station_to_airports[station] = []
        airport_to_station: Dict[Airport, Station] = {
            airport: station
            for station, airports in station_to_airports.items()
            for airport in airports
        }
        with open(const.AIRPORT_TO_RAIL_MAP, "w") as f:
            commentjson.dump({airport.code: station.code for airport, station
                              in airport_to_station.items()}, f, sort_keys=True, indent=2)

        return station_to_airports, airport_to_station

    @property
    def present_station_to_airports(self):
        return [(station, airports) for station, airports
                in self.station_to_airports.items() if station.in_graph]

    def add_distance_data_to_rail_graph(self):
        with open(const.STATION_LOCATION_DATA) as f:
            no_airport_coords = commentjson.load(f)

        for station, airports in self.present_station_to_airports:
            if station.code in no_airport_coords:
                station.lat, station.long = no_airport_coords[station.code]
            else:
                lats, longs = list(zip(*[(airport.lat, airport.long) for airport in airports]))
                station.loc_mean(lats, longs)

        self.rail.update_graph_dists()

    @property
    def unmapped_stations(self):
        return [station for station in self.rail.stations.values()
                if station not in self.station_to_airports]

    def calc_categorized_connections(self) -> pd.DataFrame:

        def find_route(line):
            index = ["orig_rail", "dest_rail", "dur_rail", "speed_rail",
                     "route", "orig_err", "dest_err"]
            orig_station, dest_station = None, None

            def error_line(is_orig: bool, is_dest: bool, value: Con):
                return line.append(pd.Series(
                    (orig_station, dest_station, None, None, None,
                     (value if is_orig else False),
                     (value if is_dest else False)),
                    index=index
                ))

            try:
                orig_station = self.airport_to_station[line.orig_fly]
                dest_station = self.airport_to_station[line.dest_fly]
            except KeyError:
                return error_line(
                    line.orig_fly not in self.airport_to_station,
                    line.dest_fly not in self.airport_to_station, Con.UNMAPPED)
            try:
                duration, route = self.rail.travel_times[orig_station][dest_station]
                # duration is in min, dist in meters, speed in km/h
                speed = line.dist_fly / 1000 / duration * 60

            except KeyError:
                if orig_station.type == "I" or dest_station.type == "I":
                    return error_line(orig_station.type == "I",
                                      dest_station.type == "I", Con.ISLAND)
                if orig_station.type in ["N", "C"] or dest_station.type in ["N", "C"]:
                    return error_line(orig_station.type in ["N", "C"],
                                      dest_station.type in ["N", "C"], Con.NO_RAIL)
                if not orig_station.in_graph or not dest_station.in_graph:
                    # this is the case if the connections json doesn't provide any data here
                    return error_line(not orig_station.in_graph,
                                      not dest_station.in_graph, Con.NOT_IN_GRAPH)
                return error_line(True, True, Con.NO_CONNECTION)

            err_msg_orig, err_msg_dest = False, False
            if not orig_station.is_standard_gauge:
                err_msg_orig = Con.WRONG_GAUGE
            if not dest_station.is_standard_gauge:
                err_msg_dest = Con.WRONG_GAUGE
            # if not orig_station.is_standard_gauge or not dest_station.is_standard_gauge:
            #     # either station is not connected with standard gauge.
            #     return error_line(not orig_station.is_standard_gauge,
            #                       not dest_station.is_standard_gauge, Con.WRONG_GAUGE)
            return line.append(pd.Series(
                (orig_station, dest_station, duration, speed, route, err_msg_orig, err_msg_dest),
                index=index
            ))

        passengers = self.fly.passenger_data
        print(f"analyzing the plane connections...")
        return passengers.apply(find_route, axis=1)

    def add_graph_effects(self, max_extendable_speed=150):
        for i in range(len(self.links)):
            link = self.links.iloc[i]
            for u, v in util.iterate_route(link.route, ordered=True):
                edge = self.rail.graph[u][v]
                if Edg.THRU not in edge:
                    edge[Edg.THRU] = []
                edge[Edg.THRU].append(link)

        for edge in self.rail.graph.edges:
            if edge[Edg.SPEED] > max_extendable_speed:
                continue

    def times_comparison(self, category, area, draw_trend=False, image_path=None, title=None,
                         step_size=100, max_duration=3000):
        links_dict: Dict[int, pd.DataFrame] = {}
        links_dict_aggr: Dict[int, LinkData] = {}
        current_standard = self.flt_links.get(category, area)
        for duration in range(max_duration, 0, -step_size):
            links_dict[duration] = current_standard[current_standard.dur_rail <= duration]
            links_dict_aggr[duration] = get_pas_conn(links_dict[duration])

        x_values = list(range(step_size, max_duration, step_size))
        if draw_trend:
            util.two_scales(
                x=x_values,
                y1=[links_dict_aggr[duration].pas for duration in x_values],
                y2=[links_dict_aggr[duration].conn for duration in x_values],
                file_path=image_path,
                xlabel="Allow Train connections below x min",
                y1label="Passenger Trips Covered", y2label="Flight Links Covered",
                y1lim=(0, util.accum_pas(self.flt_links.get(Con.POTENTIAL, area), formatted=False)),
                y2lim=(0, len(self.flt_links.get(Con.POTENTIAL, area))),
                x1_markers=[
                    (VAL[Var.PREF_DUR_MAX], DESCRIPTION[Var.PREF_DUR_MAX]),
                    (VAL[Var.ACCEPTABLE_DURATION], DESCRIPTION[Var.ACCEPTABLE_DURATION])
                ],
                title=title
            )
        return links_dict

    def calculate_gains(self, links_dict, draw_maps=False, draw_improv=False):
        def travel_time_gain(data: Dict):
            return max(
                data[Edg.SPEED] ** -1 - const.V_MAX_IMPROV ** -1, 0
            ) * data[Edg.DIST]

        ran = range(100, 2100, 100) if draw_maps else [VAL[Var.MAX_DUR_IMPROV]]
        for duration in ran:
            links_this_bucket = links_dict[duration]
            key = f"pas{duration}"
            for index in range(len(links_this_bucket)):
                row: pd.Series = links_this_bucket.iloc[index]
                for source_code, target_code in util.iterate_route(row.route):
                    edge = self.rail.graph[source_code][target_code]
                    if key not in edge:
                        edge[key] = 0
                    edge[key] += row.pas
            if draw_maps:
                stations = set(links_this_bucket.orig_rail.append(links_this_bucket.dest_rail))
                karte_pas = Karte(figsize=(8, 6), stations=stations)
                karte_pas.draw_graph_data(
                    self.rail.graph.edges.data(key), self.rail.stations,
                    min_max=(1e6, 20e6), legend_title="Passengers on link",
                    top_title=f"Passengers for Travel times <= {duration} min")
                karte_pas.store(f"{const.IMAGES_FOLDER_PAS}{key}.png", show=False)

                karte_improv = Karte(figsize=(8, 6), stations=stations)
                karte_improv.draw_graph_data([
                    (u, v, travel_time_gain(data) * data[key])
                    for u, v, data in self.rail.graph.edges.data()
                    if key in data], self.rail.stations,
                    top_title=f"Potential Travel Time Reduction Gains for Trips <= {duration} min",
                    legend_title=f"Person * Hours / km",
                    min_max=(1e6, 1e10)
                )
                karte_improv.store(f"{const.IMAGES_FOLDER_IMPROV}{key}.png", show=False)

        if draw_improv:
            key_improv = f"pas{VAL[Var.MAX_DUR_IMPROV]}"
            links_potential = self.links[self.links.dur_rail > VAL[Var.ACCEPTABLE_DURATION]].copy()

            def reduce_gain(line, u, v, t_diff):
                dur_ = line.dur_rail
                try:
                    if abs(line.route.index(u) - line.route.index(v)):
                        dur_ -= t_diff
                except ValueError:
                    pass
                return pd.Series(
                    (line.pas, line.orig_rail, line.dest_rail, line.route, dur_),
                    index=["pas", "orig_rail", "dest_rail", "route", "dur_rail"])
            edges = sorted([(u, v, travel_time_gain(data) * data[key_improv],
                             travel_time_gain(data), data[Edg.DIST])
                            for u, v, data in self.rail.graph.edges.data()
                            if data is not None and key_improv in data],
                           key=lambda tup: tup[2])
            valid_data: Dict[int, util.LinkData] = util.store_pas_conn_data_dict(links_dict,
                                                                                 store=False)
            conn_pas = valid_data[VAL[Var.MAX_DUR_IMPROV]]
            x_values, y_pas, y_conn = [0], [conn_pas.pas], [conn_pas.conn]
            x_links = ['-']
            for n, (u, v, _, time, dist) in enumerate(edges[:200]):
                x_values.append(x_values[-1] + dist)
                x_links.append(f"{u}-{v}")
                links_potential = links_potential.apply(reduce_gain, axis=1, args=(u, v, time))
                diff_conn_pas = util.get_pas_conn(
                    links_potential[links_potential.dur_rail <= VAL[Var.ACCEPTABLE_DURATION]])
                y_pas.append(y_pas[-1] + diff_conn_pas.pas)
                y_conn.append(y_conn[-1] + diff_conn_pas.conn)
                links_potential = links_potential[
                    links_potential.dur_rail > VAL[Var.ACCEPTABLE_DURATION]]

            util.two_scales(
                x=x_values, y1=y_pas, y2=y_conn, file_path=const.IMAGE_COVERED_IMPROV,
                xlabel=f"Converted km to High Speed (v={const.V_MAX_IMPROV} km/h)",
                y1label="Passenger Trips Covered", y2label="Flight Links Covered",
                y1lim=(0, util.accum_pas(self.flt_links[Con.POTENTIAL], formatted=False)),
                y2lim=(0, len(self.flt_links[Con.POTENTIAL])),
                title="High Speed construction for more night trains"
            )

    def sort_durations(self):
        def add_dur_difs(line):
            dist_rail = sum([
                self.rail.stations[line.route[i]].dist(self.rail.stations[line.route[i + 1]])
                for i in range(len(line.route) - 1)
            ])
            return pd.Series([
                line.dur_rail / line.dur_fly,
                line.dur_rail - line.dur_fly,
                dist_rail,
                dist_rail / line.dist_fly,
            ], index=["dur_prop", "dur_abs", "dist_rail", "dist_prop"]).append(line)

        diffs: pd.DataFrame = self.links.apply(add_dur_difs, axis=1)
        dur = {
            "most_dur_prop": diffs.sort_values("dur_prop", ascending=False),
            "least_dur_prop": diffs.sort_values("dur_prop", ascending=True),
            "most_dur_abs": diffs.sort_values("dur_abs", ascending=False),
            "least_dur_abs": diffs.sort_values("dur_abs", ascending=True),
            "most_dist_prop": diffs.sort_values("dist_prop", ascending=False),
            "least_dist_prop": diffs.sort_values("dist_prop", ascending=True),
        }
        return dur

    def draw_speed(self):
        karte = Karte(figsize=(8, 6), stations=[
            station for station, _ in self.present_station_to_airports])
        karte.draw_graph_data(self.rail.graph.edges.data(Edg.SPEED), stations=self.rail.stations,
                              min_max=(50, 170))
        karte.store(const.IMAGE_SPEED, show=False)

    def draw_dur(self):
        karte = Karte(figsize=(32, 24))
        for station, airports in self.present_station_to_airports:
            karte.text_point(station.lat, station.long, text=station.code, color='gray')

        for source_code, target_code, duration in self.rail.graph.edges.data(Edg.DURATION):
            color = np.random.rand(3, )  # random color
            source = self.rail.stations[source_code]
            target = self.rail.stations[target_code]
            karte.line(source.lat, source.long, target.lat, target.long,
                       color=util.add_alpha(color, 0.4))
            karte.text_point(lat=source.lat, long=source.long, lat2=target.lat, long2=target.long,
                             text=f"{duration}", color=color)
        karte.store(const.IMAGE_DURATION, show=False)


if __name__ == "__main__":
    k = Kiss(Fly(1), Rail())
    # k.draw_speeds()
    # k.draw_dur()
    l = k.times_comparison(area=None, category=Con.VALID, draw_trend=False,
                           image_path=const.IMAGE_COVERED,
                           title="Links Covered (standard gauge)")
    k.times_comparison(area=None, category=Con.ALL_GAUGE, image_path=const.IMAGE_COVERED_ALL_GAUGE,
                       title="Links Covered regardless of gauge", draw_trend=False)
    k.times_comparison(area=Con.EEA, category=Con.VALID, draw_trend=False,
                       image_path=const.IMAGE_COVERED_EEA,
                       title="Links within European Economic Area")
    k.times_comparison(area=Con.CORE, category=Con.VALID, draw_trend=False,
                       image_path=const.IMAGE_COVERED_CORE,
                       title="Links within 'Core' Europe")

    k.calculate_gains(l, draw_maps=False)
    k.sort_durations()

    print("done")
