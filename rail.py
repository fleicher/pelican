from typing import List, Dict, Tuple, Optional, Union

import commentjson
import networkx as nx
import pandas as pd

from claz import const
from claz import util
from claz.const import Edg
from claz.station import Station
from claz.util import Karte

StationNetwork = Optional[Dict[Station, Dict[Station, Tuple[float, List[Station]]]]]


class Rail:
    def __init__(self):
        with open(const.STATION_CODE_DATA, "r") as f:
            self.station_list: List[Station] = [Station(station_name, station_code)
                                                for station_name, station_code in
                                                commentjson.load(f).items()]
        self.stations: Dict[str, Station] = {station.code: station for station in self.station_list
                                             if station.code is not None}
        self.graph: nx.Graph = self.create_network()
        self.travel_times: StationNetwork = self.calc_travel_lengths()
        self.travel_distances: StationNetwork = None
        self.unnecessary_links = self.find_unnecessary_links(remove=True)
        self.generate_links_json()

    def create_network(self) -> nx.Graph:
        graph = nx.Graph()
        with open(const.TIMES_DATA, "r") as f:
            travel_times = commentjson.load(f)["times"]
            # [
            #   ["bel", "dub", 2.10],
            #   ["dub", "cok", 2.45],
            #           ...
            # ]

        # this is necessary, because the json files store the travel durations as
        # 2.45 -> 2 hrs 45 min
        def to_minutes(uvw):
            u, v, w = uvw
            return u, v, int(w // 1 * 60 + w % 1 * 100)

        with open(const.GAUGE_DATA) as f:
            gauge_types: Dict[str, List[str]] = commentjson.load(f)
            # {
            #   "Iberian": ["por", "lis", ...],
            #   "Russian": ["mos", ...]
            # }
            city2gauge = {code: gauge_type for gauge_type, cities in gauge_types.items()
                          for code in cities}

        times_in_min = [to_minutes(time) for time in travel_times]
        graph.add_weighted_edges_from(times_in_min)

        stations_in_the_graph = list(nx.nodes(graph))
        number_of_stations_in_graph = 0
        for station_code, station in self.stations.items():
            station.in_graph = station_code in stations_in_the_graph
            number_of_stations_in_graph += int(station.in_graph)
            station.is_standard_gauge = station_code not in city2gauge
        print(f"{number_of_stations_in_graph} of {len(self.stations)} train stations "
              f"are added to the graph. [internal info, "
              f"the rest are islands, skipped for brevity, ...")

        for source_code, target_code in graph.edges:
            gauge = "standard"
            if source_code in city2gauge:
                gauge = city2gauge[source_code]
            if target_code in city2gauge:
                gauge = city2gauge[target_code]
            graph[source_code][target_code][Edg.GAUGE] = gauge
        return graph

    def update_graph_dists(self):
        """ for this the stations' lat,long have to be set."""

        for source_code, target_code, duration in self.graph.edges.data('weight'):
            source, target = self.stations[source_code], self.stations[target_code]
            dist = Karte.distance(source.lat, source.long, target.lat, target.long)
            self.graph[source_code][target_code][Edg.DIST] = dist
            self.graph[source_code][target_code][Edg.SPEED] = dist / 1000 / duration * 60
        self.travel_distances = self.calc_travel_lengths(weight=Edg.DIST)

    def connected_components(self):
        return nx.connected_components(self.graph)

    def sum_for_weights_on_route(self, route: List[Union[Station, str]],
                                 weight: str = Edg.DURATION):
        def get_code(c: Union[str, Station]):
            if isinstance(c, Station):
                return c.code
            return c
        return sum([self.graph[get_code(route[n])][get_code(route[n + 1])][weight]
                    for n in range(len(route) - 1)])

    def calc_travel_lengths(self, weight=Edg.DURATION):
        lengths: Dict[Station, Dict[Station, Tuple[int, List[Station]]]] = {}
        # print("getting rail graph network lengths...")
        for source_code, targets in nx.shortest_path(self.graph, weight=weight).items():
            targets2 = {}
            for target_code, route in targets.items():
                length = self.sum_for_weights_on_route(route, weight)
                targets2[self.stations[target_code]] = (length, route)
            lengths[self.stations[source_code]] = targets2
        return lengths

    def find_unnecessary_links(self, remove=True):
        columns = ["source", "target", "dur_short", "dur_dir", "prop_longer", "route"]
        useless_links_list: List[Tuple[Station, Station, float, float, float, List[Station]]] = []
        # unnecessary_links: List[Tuple[Station, Station, float, float, float, List[Station]]] = []

        for source, targets in self.travel_times.items():
            source_neighbors: Dict[str, Dict[str, float]] = dict(self.graph[source.code])
            for target, (shortest_duration, route) in targets.items():
                if len(route) < 3 or target.code not in source_neighbors:
                    continue  # a direct route has 2 elements: [source, target]
                direct_duration = source_neighbors[target.code][Edg.DURATION]
                link = (source, target, shortest_duration, direct_duration,
                        direct_duration / shortest_duration, route)
                if direct_duration > shortest_duration:
                    useless_links_list.append(link)
                    if remove:
                        self.graph.remove_edge(source.code, target.code)
        useless_links = pd.DataFrame(useless_links_list, columns=columns).sort_values(
            "prop_longer", ascending=False)
        if remove:
            print(f"Eliminated {len(useless_links)} direct links from the graph "
                  f"that have shorter alternatives")
        return useless_links

    def generate_links_json(self):
        links = []
        for u, v, data in self.graph.edges(data=True):
            links.append({
                "u": u,
                "v": v,
                "t": data[Edg.DURATION],
                "g": data[Edg.GAUGE]
            })
        stations = []
        for station in self.station_list:
            stations.append({
                "code": station.code,
                "name": station.name,
                "valid": station.code is not None,
            })
        with open(const.LINKS_DATA, "w") as f:
            commentjson.dump({
                "links": links,
                "stations": stations
            }, f, sort_keys=True, indent=2)

    def find_detours(self):
        distance_detours: List[Tuple[
            Station, Station, float, float, float, float, float, float,
            List[Station], List[Station]]] = []
        for source, targets in self.travel_times.items():
            for target, (dur_fastest, route_fastest) in targets.items():
                if source.code > target.code:
                    continue  # symmetric graph, only take symmetric connections
                dist_shortest, route_shortest = self.travel_distances[source][target]
                if "par" in route_fastest and "par" not in route_shortest:
                    continue  # this is ok, going via Paris is often faster.
                if route_fastest != route_shortest:
                    # print(f"shortest != fastest route: {source} -> {target}")
                    dist_fastest = self.sum_for_weights_on_route(route_fastest, Edg.DIST)
                    dur_shortest = self.sum_for_weights_on_route(route_shortest, Edg.DURATION)
                    distance_detours.append((
                        source, target,
                        dur_fastest, dur_shortest, dur_shortest - dur_fastest,
                        dist_fastest, dist_shortest, dist_fastest - dist_shortest,
                        route_fastest, route_shortest))
        df = pd.DataFrame(distance_detours, columns=[
            "source", "target",
            "dur_fastest", "dur_shortest", "dur_diff",
            "dist_fastest", "dist_shortest", "dist_diff",
            "route_fastest", "route_shortest"])
        df = df.astype({'dist_fastest': 'int32', 'dist_shortest': 'int32', 'dist_diff': 'int32'})
        util.table_to_file(df.sort_values("dur_diff", ascending=False),
                           const.TABLE_DETOURS_DURATION)
        util.table_to_file(df.sort_values("dist_diff",
                                          ascending=False), const.TABLE_DETOURS_DISTANCE)
        return df


if __name__ == "__main__":
    r = Rail()
