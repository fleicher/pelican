from typing import Dict

import commentjson
import pandas as pd

from claz import const
from claz.const import Con, TABLES_PATH, store_variable, VAL, Var, VARIABLE_TEXT, VARIABLES
from claz.util import get_pas_conn, generate_table

ERROR_TYPES = [Con.ISLAND, Con.NO_CONNECTION, Con.UNMAPPED, Con.NO_RAIL, Con.NOT_IN_GRAPH,
               Con.WRONG_GAUGE]
AREAS = [Con.EEA, Con.CORE]


class FilteredLinks:
    def __init__(self, all_links: pd.DataFrame):
        self.all_links = all_links
        all_gauges = ~pd.isna(all_links.dur_rail)
        with open(const.AIRPORT_AREAS, "r") as f:
            port_area_map = commentjson.load(f)
        areas_map = {
            area: {prefix for prefix, data in port_area_map.items() if area in data[0]}
            for area in AREAS
        }
        for area in AREAS:
            countries = [data[1] for data in port_area_map.values() if area in data[0]]
            with open(VARIABLES + VARIABLE_TEXT + f"countries_{area}", "w") as f:
                f.write(", ".join(countries))

        self.categorization = {
            # any air link, regardless if rail station exists
            Con.ALL: all_links.pas != "fake_value",
            # all air links that have some some existing rail connection
            Con.ALL_GAUGE: all_gauges,
            # all air links that have a valid same-gauge rail connection
            Con.VALID: all_gauges & (all_links.orig_err == False) & (all_links.dest_err == False),
            # links that could  be connected, when excluding islands
            Con.POTENTIAL: (~all_links.orig_err.isin((Con.ISLAND, Con.NO_CONNECTION))
                            & ~all_links.dest_err.isin((Con.ISLAND, Con.NO_CONNECTION))),
            # rail time under the acceptable duration
            Con.NOT_TOO_LONG: all_links.dur_rail <= VAL[Var.ACCEPTABLE_DURATION],
            Con.TOO_LONG: VAL[Var.ACCEPTABLE_DURATION] < all_links.dur_rail,
            Con.REALLY_LONG: ~~(VAL[Var.PREF_DUR_MAX] < all_links.dur_rail) &
                             ~~(all_links.dur_rail <= VAL[Var.ACCEPTABLE_DURATION]),
            Con.REALLY_SHORT: all_links.dur_rail < VAL[Var.PREF_DUR_MIN],
            Con.PREF_LENGTH: ~~(VAL[Var.PREF_DUR_MIN] <= all_links.dur_rail) &
                             ~~(all_links.dur_rail <= VAL[Var.PREF_DUR_MAX])
        }

        for err_type in ERROR_TYPES:
            self.categorization[err_type] = (all_links.orig_err == err_type) \
                                            | (all_links.dest_err == err_type)

        for area in AREAS:
            def is_in_area(row):
                return row.code[:2] in areas_map[area]

            self.categorization[area] = all_links.dest_fly.apply(is_in_area) \
                                        & all_links.orig_fly.apply(is_in_area)
        self.html = ""

        for category in (Con.VALID, Con.NOT_TOO_LONG, Con.TOO_LONG, Con.REALLY_LONG,
                         Con.PREF_LENGTH, Con.REALLY_SHORT):
            table = self.get(category).drop(
                columns=["orig_rail", "dest_rail", "orig_err", "dest_err"]).rename(
                columns={"orig_fly": "Origin", "dest_fly": "Destination", "pas": "Passengers",
                         "dist_fly": "Beeline [m]", "dur_fly": "t flight (min)",
                         "dur_rail": "t rail (min)", "speed_rail": "avg v rail (km/h)",
                         "route": "Rail stations passed"
            })
            generate_table(table, category + ".html")

    def store_pas_conn_data_dict(self):
        for key in self.categorization.keys():
            try:
                store_variable(const.VARIABLE_DESC + key, const.DESCRIPTION[key])
            except KeyError:
                pass
            pas_conn = get_pas_conn(self[key])
            store_variable(const.VARIABLE_PAS + key, pas_conn.pas_formatted)
            store_variable(const.VARIABLE_LINK + key, pas_conn.conn)

    def __getitem__(self, item):
        return self.all_links[self.categorization[item]]

    def get(self, category, area=None):
        selector = self.categorization[category].copy()
        if area:
            selector &= self.categorization[area]
        return self.all_links[selector]

    def print(self, areas):
        categories = [Con.ALL, Con.ISLAND, Con.NOT_IN_GRAPH, Con.NO_CONNECTION, Con.POTENTIAL,
                      Con.UNMAPPED, Con.NO_RAIL, Con.ALL_GAUGE, Con.WRONG_GAUGE, Con.VALID,
                      Con.TOO_LONG, Con.NOT_TOO_LONG, Con.REALLY_LONG, Con.PREF_LENGTH,
                      Con.REALLY_SHORT]

        def get_row(category):
            row = {}
            for area in areas:
                links = self.get(category, area)
                row.update({
                    (area, Con.PAS): "{:.1f}M".format(sum(links["pas"]) / 1000000),
                    (area, Con.CONNS): len(links),
                })
            row["description"] = const.DESCRIPTION[category]
            return row

        data = pd.DataFrame([get_row(category) for category in categories], index=categories)

        self.html += (
                "<table style='border-collapse:collapse'>\n"
                "<thead>\n<tr>\n\t<th>Category</th>\n" +
                "".join([f"\t<th>{area if area else 'europe'}</th>\n" for area in areas]) +
                "\t<th>Description</th>\n</tr>\n</thead>\n<tbody>\n")

        def print_row(category, prefix="- ", border=0):
            row = data.loc[category]
            if border == 1:
                print("=" * 20)
            style = "" if not border else f"font-weight: bold; border-top: {border}px solid black"
            self.html += (
                    f"<tr style='{style}'>\n"
                    f"\t<th><i>{prefix}&nbsp;{category}</i></th>\n" +
                    "".join([f"\t<td>{row[(area, Con.PAS)]}({row[(area, Con.CONNS)]})</td>\n"
                             for area in areas]) +
                    f"\t<td>{row['description']}</td>\n</tr>\n")

            print(f"{prefix}\t"
                  + "\t|".join([f"{row[(area, Con.PAS)]}({row[(area, Con.CONNS)]})"
                                for area in areas])
                  + f"\t||{row['description']}")

        print()
        print("****" + "\t\t".join([str(area) for area in areas]) + "****")
        print_row(Con.ALL, prefix="")
        print_row(Con.ISLAND)
        print_row(Con.NOT_IN_GRAPH)
        print_row(Con.NO_CONNECTION)
        print_row(Con.POTENTIAL, prefix="=")
        print_row(Con.UNMAPPED)
        print_row(Con.NO_RAIL)
        print_row(Con.ALL_GAUGE, prefix="=")
        print_row(Con.WRONG_GAUGE)
        print_row(Con.VALID, prefix="=", border=1)
        print_row(Con.TOO_LONG, prefix="-")
        print_row(Con.NOT_TOO_LONG, prefix="=", border=1)
        print_row(Con.REALLY_LONG, prefix="*")
        print_row(Con.PREF_LENGTH, prefix="*")
        print_row(Con.REALLY_SHORT, prefix="*")
        self.html += "</tbody>\n</table>\n"
        filepath = TABLES_PATH + "filter.html"
        with open(filepath, "w") as f:
            f.write(self.html)

    def debug_non_mapped_links(self):
        # the following can be used to debug the non mapped links
        def sort_agg(df, mode_: str):
            return df.groupby(
                [f"orig_{mode_}"]).agg({'pas': ['sum', "mean", "count"]}).sort_values(
                ("pas", "sum"), ascending=False)

        aggr_orig: Dict[Con, pd.DataFrame] = {
            Con.TOO_LONG: sort_agg(self[Con.TOO_LONG], "rail")
        }  # can be used for debugging.
        for err_type in ERROR_TYPES:
            mode = 'fly' if err_type in [Con.UNMAPPED, Con.NO_RAIL, Con.ISLAND] else 'rail'
            aggr_orig[err_type] = sort_agg(self.all_links[self.all_links.orig_err == err_type],
                                           mode)
        return aggr_orig
