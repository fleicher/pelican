import os
from typing import Union


class Con:
    """how the connections can be categorized, they do overlap!"""
    ALL = "all"
    EEA = "eea"
    CORE = "core"

    POTENTIAL = "potential"
    UNMAPPED = "unmapped"
    NO_RAIL = "no_rail"
    ISLAND = "island"
    NOT_IN_GRAPH = "not_in_graph"
    WRONG_GAUGE = "wrong_gauge"
    NO_CONNECTION = "no_connection"

    ALL_GAUGE = "all_gauge"
    VALID = "valid"
    TOO_LONG = "too_long"
    NOT_TOO_LONG = "short_enough"
    REALLY_LONG = "really_long"
    PREF_LENGTH = "pref_length"
    REALLY_SHORT = "really_short"

    CONNS = "connections"
    PAS = "passengers"


class Var(Con):
    ALL_LINKS = "all_links"
    EUROPEAN_AIRPORTS = "european_airports"
    ACCEPTABLE_DURATION = "acceptable_duration"
    PREF_DUR_MIN = "pref_min_dur"
    PREF_DUR_MAX = "pref_dur_max"
    MAX_V_IMPROV = "max_v_improv"
    MAX_DUR_IMPROV = "max_dur_improv"


VAL = {
    Var.ACCEPTABLE_DURATION: 900,  # 15 hrs
    Var.PREF_DUR_MAX: 600,  # 10hrs
    Var.PREF_DUR_MIN: 360,  # 6hrs

    Var.MAX_V_IMPROV: 170,  # km/h
    Var.MAX_DUR_IMPROV: 1400,
}

DESCRIPTION = {
    Con.ALL: "all flight links",
    Con.EEA: "European Economic Area links",
    Con.CORE: "Main Western/Central European Area",

    Con.POTENTIAL: "connection not to island",
    Con.ISLAND: "the flight goes to an island",
    Con.NO_CONNECTION: "no rail connection found",

    Con.UNMAPPED: "no idea where the airport lies next to",
    Con.NO_RAIL: "we know there are no trains there",
    Con.NOT_IN_GRAPH: ">= 1 station is not in graph",
    Con.WRONG_GAUGE: ">= 1 station is not in standard gauge",

    Con.VALID: "have potential existing train connections",
    Con.TOO_LONG: f"duration too long (>{VAL[Var.ACCEPTABLE_DURATION]} min)",
    Con.NOT_TOO_LONG: f"convertible to (night) trains (<{VAL[Var.ACCEPTABLE_DURATION]} min)",
    Con.REALLY_LONG: f"really long night train ({VAL[Var.PREF_DUR_MAX]} < x "
    f"< {VAL[Var.ACCEPTABLE_DURATION]} min)",
    Con.PREF_LENGTH: f"good night train length ({VAL[Var.PREF_DUR_MIN]} < x "
    f"< {VAL[Var.PREF_DUR_MAX]} min)",
    Con.REALLY_SHORT: f"day train (<{VAL[Var.PREF_DUR_MIN]} min)",
    Var.PREF_DUR_MAX: "preferable max duration",
    Var.PREF_DUR_MIN: "preferable min duration",
    Var.ACCEPTABLE_DURATION: "max duration night train",

    Con.ALL_GAUGE: "exiting connections regardless of gauge",

    Con.CONNS: "connections",
    Con.PAS: "passengers",
}


class Edg:
    DURATION = "weight"
    SPEED = "speed"
    DIST = "dist"
    GAUGE = "gauge"
    THRU = "thru"


LAT_RANGE = (35.0, 65.0)  # y-axis
LONG_RANGE = (-11.0, 39.0)  # x-axis

V_MAX_IMPROV = 170  # km/h assume we can't build beeline connections faster than that.

COORDS_DATA = 'content/data_air/airport-codes.txt'
COORDS_PICKLE = 'store/coords.pickle'
AIR_DATA_FOLDER = 'content/data_air/'
AIR_DATA_PICKLE = 'store/data_all.pickle'
AIRPORT_AREAS = 'content/data/area.json'
STATION_CODE_DATA = 'content/data/station_codes.json'
TIMES_DATA = 'content/data/rail_travel_times.json'
RAIL_TO_FLY_MAP_CORRECTION_DATA = 'content/data/mapping_names_correction.json'
STATION_LOCATION_DATA = 'content/data/station_location.json'

GAUGE_DATA = 'content/data/gauge.json'
AIRPORT_TO_RAIL_MAP = 'content/generated/data/airport2station.json'

IMAGES_AIR_CODES = 'content/images/airports_codes_all.png'
IMAGE_SPEED = "content/images/speed.png"
IMAGE_DURATION = "content/images/duration.png"
IMAGE_COVERED = "content/images/covered.png"
IMAGE_COVERED_ALL_GAUGE = "content/images/covered_all_gauge.png"
IMAGE_COVERED_EEA = "content/images/covered_eea.png"
IMAGE_COVERED_CORE = "content/images/covered_core.png"

IMAGE_COVERED_IMPROV = "content/images/covered_improv.png"
IMAGES_FOLDER_PAS = "content/images/pas/"
IMAGES_FOLDER_IMPROV = "content/images/improv/"

TABLES_PATH = 'content/generated/tables/'
TABLE_DETOURS_DURATION = 'content/generated/detours_duration.html'
TABLE_DETOURS_DISTANCE = 'content/generated/detours_distance.html'
LINKS_DATA = 'content/generated/links.json'

VARIABLES = 'content/generated/vars/'
VARIABLE_PAS = 'pas/'
VARIABLE_LINK = 'link/'
VARIABLE_DESC = 'desc/'
VARIABLE_TEXT = 'text/'


def store_variable(key_: str, value: Union[str, float]):
    os.makedirs(VARIABLES, exist_ok=True)
    with open(VARIABLES + key_, "w") as f:
        f.write(str(value))
    return value


for key, val in VAL.items():
    store_variable(key, val)
