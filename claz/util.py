import collections
import os
import pickle
from typing import List, Iterable, Tuple, Union, Any, Dict

import cartopy.crs as c_crs
import cartopy.feature
import matplotlib.patches as m_patches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import unidecode
from pyproj import Geod

from claz import const
from claz.const import Con, TABLES_PATH, store_variable


def get_eu_map(figsize=(10, 6)):
    projection = c_crs.AlbersEqualArea(np.mean(const.LONG_RANGE), np.mean(const.LAT_RANGE))
    plt.figure(figsize=figsize)
    ax = plt.axes(projection=projection)
    ax.set_extent(const.LONG_RANGE + const.LAT_RANGE)

    ax.add_feature(cartopy.feature.OCEAN)
    ax.add_feature(cartopy.feature.LAND, edgecolor='black')
    ax.add_feature(cartopy.feature.LAKES, edgecolor='black')
    return ax, projection


def to_ascii(name):
    if name != name:  # nan
        return ""
    return unidecode.unidecode(name).lower().translate(
        {ord(ch): '' for ch in ["'", "-", ".", " "]})


def load_pickle(file_path):
    # print("loading pickle codes from", os.path.join(os.getcwd(), file_path))
    with open(file_path, 'rb') as handle:
        return pickle.load(handle)


def dump_pickle(file_path, data):
    print("storing pickle to", os.path.join(os.getcwd(), file_path))
    with open(file_path, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def table_to_file(df: pd.DataFrame, file_path: str, top_rows=20):
    with open(file_path, "w") as f:
        f.write(df[:top_rows].to_html(max_rows=top_rows))


class Karte:
    g = Geod(ellps='WGS84')
    cm = plt.cm.jet

    def __init__(self, figsize=(10, 6), stations: Iterable = None):
        self.projection = c_crs.AlbersEqualArea(np.mean(const.LONG_RANGE),
                                                np.mean(const.LAT_RANGE))
        plt.figure(figsize=figsize)
        self.ax = plt.axes(projection=self.projection)
        self.ax.set_extent(const.LONG_RANGE + const.LAT_RANGE)

        self.ax.add_feature(cartopy.feature.OCEAN)
        self.ax.add_feature(cartopy.feature.LAND, edgecolor='black')
        self.ax.add_feature(cartopy.feature.LAKES, edgecolor='black')

        if stations is not None:
            for station in stations:
                self.text_point(station.lat, station.long, text=station.code)

    def text_point(self, lat, long, *, text=None, color="black",
                   show_point=True, lat2=None, long2=None):
        x, y = self.projection.transform_point(long, lat, c_crs.Geodetic())
        if lat2 is not None and long2 is not None:
            x2, y2 = self.projection.transform_point(long2, lat2, c_crs.Geodetic())
            x, y = (x + x2) / 2, (y + y2) / 2
        if show_point:
            plt.plot(x, y, c=color)
        if text is not None:
            plt.text(x, y, s=text, c=color)

    def line(self, lat1: float, long1: float, lat2: float, long2: float, *, color="black"):
        x1, y1 = self.projection.transform_point(long1, lat1, c_crs.Geodetic())
        x2, y2 = self.projection.transform_point(long2, lat2, c_crs.Geodetic())
        plt.plot([x1, x2], [y1, y2], c=color)

    def draw_graph_data(self, edges_data_view, stations,
                        min_max: Tuple[float, float], top_title: str = None,
                        legend_title: str = None, sub_title: str = None):
        data_values = []
        lat1s, long1s, lat2s, long2s = [], [], [], []
        for source_code, target_code, value in edges_data_view:
            if value is None:
                continue
            data_values.append(value)
            source = stations[source_code]
            target = stations[target_code]
            lat1s.append(source.lat)
            long1s.append(source.long)
            lat2s.append(target.lat)
            long2s.append(target.long)
        self.lines(lat1s, long1s, lat2s, long2s,
                   colors=Karte.color_list(data_values, min_=min_max[0],
                                           max_=min_max[1], title=legend_title))
        if top_title is not None:
            plt.title(top_title)
        if sub_title is not None:
            self.ax.set_x_label(sub_title)

    def lines(self, lat1s: List[float], long1s: List[float],
              lat2s: List[float], long2s: List[float], colors: List[str] = None):

        xyz_1 = self.projection.transform_points(c_crs.Geodetic(),
                                                 np.array(long1s), np.array(lat1s))
        xyz_2 = self.projection.transform_points(c_crs.Geodetic(),
                                                 np.array(long2s), np.array(lat2s))
        xs = np.hstack((xyz_1[:, 0].reshape((-1, 1)), xyz_2[:, 0].reshape((-1, 1))))
        ys = np.hstack((xyz_1[:, 1].reshape((-1, 1)), xyz_2[:, 1].reshape((-1, 1))))
        # this has the form: [[x11, x12], [x21, x22], ...]

        # to plot multiple lines simultaneously, the input needs to be in the format
        # plot([x11, x12], [y11, y12], [x21, x22], [y21, y22], [x31, x32], [y31, y32], ... )
        drawn_lines_unflat = [(xs[i], ys[i], ("k-" if colors is None else colors[i])) for i in
                              range(len(xs))]
        self.ax.plot(*[el for line in drawn_lines_unflat for el in line])

    @staticmethod
    def store(file_path, *, show=True):
        print("saving figure " + file_path)
        plt.savefig(file_path, transparent=True)
        if show:
            plt.show()
        plt.close()

    @staticmethod
    def color(value, rng=(0, 250)):
        return Karte.cm((value - rng[0]) / (rng[1] - rng[0]))

    @staticmethod
    def color_list(weights: List[float], *, min_: float, max_: float, title=None) -> List[str]:
        li = ['k', 'r', 'y', 'g', 'c', 'b', 'm']
        bounds = np.linspace(min_, max_, len(li) - 1)
        patches = [
                      m_patches.Patch(color=li[i], label=f"<{bound:.0f}")
                      for i, bound in enumerate(bounds)
                  ] + [m_patches.Patch(color=li[-1], label=f"≥{bounds[-1]:.0f}")]
        plt.legend(handles=patches, title=title)
        return [li[i] for i in np.digitize(weights, bounds, right=True)]

        # rng, step = max_ - min_, (max_ - min_) / len(li)
        # # 49  -> 0, 50  -> 1, 80  -> 2, 110 -> 3,
        # # 140 -> 4, 170 -> 5, 199 -> 5, 200 -> 6
        # patches = [
        #     mpatches.Patch(color=li[i], label=f"<{min_ + step *i:.0f}") for i in range(len(li)-1)
        # ] + [mpatches.Patch(color=li[-1], label=f"≥{min_ + step * (len(li)-2):.0f}")]
        # plt.legend(handles=patches)
        #
        # return [li[int(min(max((w-min_+step) / rng * len(li), 0), len(li)-1))] for w in weights]

    """
    the distance in meters
    https://clouds.eos.ubc.ca/~phil/courses/atsc301/html/cartopy_mapping_pyproj.html
    """

    @staticmethod
    def distance(lat1: float, long1: float, lat2: float, long2: float):
        _, _, dist = Karte.g.inv(long1, lat1, long2, lat2)
        return dist
        # return self.geo.geometry_length(LineString(longlat_list))

    @staticmethod
    def save(file_path):
        plt.savefig(file_path)


def add_alpha(color: np.array, alpha=1) -> np.array:
    return np.hstack((color, alpha))


def iterate_route(route: List[str], ordered=False):
    index = 0
    while index < len(route) - 1:
        index += 1
        if ordered:
            yield order((route[index - 1], route[index]))
        else:
            yield route[index - 1], route[index]


def order(tup: Tuple[Any, Any]) -> Tuple:
    if tup[0] < tup[1]:
        return tup
    return tuple(tup[::-1])


def two_scales(x: List, y1, y2, file_path, xlabel=None, y1label=None, y2label=None,
               y1lim=None, y2lim=None,
               x1_markers: List[Tuple[float, str]] = None,
               y1_markers: List[Tuple[float, str]] = None,
               y2_markers: List[Tuple[float, str]] = None,
               title: str = None):
    fig, ax1 = plt.subplots()

    color = 'tab:red'
    if title is not None:
        ax1.set_title(title)
    if xlabel is not None:
        ax1.set_xlabel(xlabel)
    if y1label is not None:
        ax1.set_ylabel(y1label, color=color)
    ax1.plot(x, y1, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    if y1lim is None:
        y1lim = (0, max(y1))
    ax1.set_ylim(y1lim)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:blue'
    if y2label is not None:
        ax2.set_ylabel(y2label, color=color)  # we already handled the x-label with ax1
    ax2.plot(x, y2, color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    if y2lim is None:
        y2lim = (0, max(y2))
    ax2.set_ylim(y2lim)

    def draw_text_line(value, desc, horizontal=True, use_ax1=True, marker="k--"):
        ax = ax1 if use_ax1 else ax2
        xx = [x[0], x[-1]] if horizontal else [value, value]
        yy = [value, value] if horizontal else ax.get_ylim()
        ax.plot(xx, yy, marker)
        ax.text(sum(xx) / 2, sum(yy) / 2, desc,
                horizontalalignment='center' if horizontal else 'right',
                verticalalignment='bottom' if horizontal else 'center',
                rotation=None if horizontal else 'vertical')

    if x1_markers is not None:
        for (x_value, description) in x1_markers:
            assert x_value in x
            draw_text_line(x_value, description, horizontal=False, marker="k-")
            y_value = y1[x.index(x_value)]
            draw_text_line(y_value, f"{y_value / y1lim[1] * 100:0.0f}%")

    if y1_markers is not None:
        for (y1_value, description) in y1_markers:
            draw_text_line(y1_value, description)
    if y2_markers is not None:
        for (y2_value, description) in y2_markers:
            draw_text_line(y2_value, description, use_ax1=False)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    print(f"Saving figure to {file_path}")
    plt.savefig(file_path, transparent=True)
    plt.show()


LinkData = collections.namedtuple('ConnPas', 'conn pas pas_formatted desc')


def accum_pas(links: pd.DataFrame, formatted=False):
    value = sum(links["pas"])
    if not formatted:
        return value
    return "{:.1f}M".format(value / 1000000)


KeyType = Union[str, Con, int]


def get_pas_conn(links: pd.DataFrame, desc: str = None):
    conn = len(links)
    pas = sum(links["pas"])
    pas_formatted = "{:.1f}M".format(pas / 1000000)
    return LinkData(conn=conn, pas=pas, pas_formatted=pas_formatted, desc=desc)


def store_pas_conn_data_dict(links_dict: Dict[KeyType, pd.DataFrame],
                             value="", store=True) -> Dict[KeyType, LinkData]:
    data = {}
    for key, links in links_dict.items():
        try:
            description = const.DESCRIPTION[key]
            if store:
                store_variable(const.VARIABLE_DESC + value + key, description)
        except KeyError:
            description = None
        data[key] = get_pas_conn(links, desc=description)
        if store:
            store_variable(const.VARIABLE_PAS + value + key, data[key].pas_formatted)
            store_variable(const.VARIABLE_LINK + value + key, data[key].conn)

    return data


def generate_table(df: pd.DataFrame, key: str):
    with open(TABLES_PATH + key, "w") as f:
        f.write("\n".join(
            ['<script src="https://train.leicher.eu/assets/js/sortable.min.js"></script>',
             '<table class="sortable-theme-bootstrap" data-sortable>']
            + df.to_html().split("\n")[1:]))
