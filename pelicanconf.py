#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Florian'
SITENAME = 'Night Trains'
SITEURL = ''  # overwritten by publishconf.py

PATH = 'content'
JINJA2CONTENT_TEMPLATES = ['templates', 'images', 'generated']
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = 'en'

THEME = 'themes/pelican-alchemy/alchemy'
# THEME = 'themes/attila'
# THEME = 'themes/pelican-twitchy'
# THEME = 'themes/pelicanyan'

PLUGINS = ['jinja2content']

HOME_COVER = 'https://casper.ghost.org/v1.0.0/images/welcome.jpg'
SITEDESCRIPTION = 'sample blog'
SITESUBTITLE = 'An Alternative to European Flights?'

# from: https://unsplash.com/photos/El5zuQAtfeo
SITEIMAGE = '/images/old_train.jpg height=150'
DESCRIPTION = 'A functional, clean, responsive theme for Pelican. Heavily ' \
              'inspired by crowsfoot and clean-blog, powered by Bootstrap.'
HIDE_AUTHORS = True
THEME_CSS_OVERRIDES = ['theme/css/oldstyle.css', '/assets/css/sortable-theme-bootstrap.css']

PLUGIN_PATHS = [
    # 'm.css/plugins',
    'plugins'
]
PLUGINS += ['m.htmlsanity', 'm.images']
M_IMAGES_REQUIRE_ALT_TEXT = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('EU Air Data', 'https://ec.europa.eu/eurostat/data/database?node_code=avia_tf_apal'),
    ('Rio2Rome', 'https://www.rome2rio.com/'),
    ('Trainline', 'https://public.opendatasoft.com/explore/dataset/european-train-stations/api/?q=Paris&lang=FR')
)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

STATIC_PATHS = ['assets', 'images', 'data', 'data_air']  # we just take pages and images
# PAGE_EXCLUDES = ['images', 'templates', ]
ARTICLE_EXCLUDES = [] + JINJA2CONTENT_TEMPLATES

EXTRA_PATH_METADATA = {
    'assets/robots.txt': {'path': 'robots.txt'},
    'images/favicon.ico': {'path': 'favicon.ico'},
    'assets/CNAME': {'path': 'CNAME'}
}

ICONS = (
    ('github', 'https://gitlab.com/fleicher/pelican'),
)

# read these default parameters
# http://docs.getpelican.com/en/3.6.3/settings.html
# https://github.com/getpelican/pelican-plugins/tree/master/photos
# https://github.com/getpelican/pelican-plugins
# https://github.com/ran404/pelican-plotly
# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
