pyproj
networkx
unidecode
matplotlib
cartopy
numpy
commentjson
pandas
pelican ~= 4.1.0
markdown <= 2.11.3
typogrify
Pillow
