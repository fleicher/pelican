Abbreviations:
	PAS_BRD	Passengers on board
	PAS_BRD_ARR	Passengers on board (arrivals)
	PAS_BRD_DEP	Passengers on board (departures)
	PAS_CRD	Passengers carried
	PAS_CRD_ARR	Passengers carried (arrival)
	PAS_CRD_DEP	Passengers carried (departures)
	ST_PAS	Passengers seats available
	ST_PAS_ARR	Passengers seats available (arrivals)
	ST_PAS_DEP	Passengers seats available (departures)
	CAF_PAS	Commercial passenger air flights
	CAF_PAS_ARR	Commercial passenger air flights (arrivals)
	CAF_PAS_DEP	Commercial passenger air flights (departures)

Unit
	PAS	Passenger
	FLIGHT	Flight
	SEAT	Seats and berths





Air transport measurement – passengers (avia_pa), freight and mail (avia_go), airport traffic (avia_tf) and regional statistics (avia_rg)

Reference Metadata in Euro SDMX Metadata Structure (ESMS)

Compiling agency: Eurostat, the Statistical Office of the European Union


Eurostat metadata
Reference metadata
1. Contact
2. Metadata update
3. Statistical presentation
4. Unit of measure
5. Reference Period
6. Institutional Mandate
7. Confidentiality
8. Release policy
9. Frequency of dissemination
10. Accessibility and clarity
11. Quality management
12. Relevance
13. Accuracy
14. Timeliness and punctuality
15. Coherence and comparability
16. Cost and Burden
17. Data revision
18. Statistical processing
19. Comment
Related Metadata
Annexes (including footnotes)
National metadata
National reference metadata
National metadata produced by countries and released by Eurostat

BelgiumBulgariaCzechiaGermanyEstonia

IrelandCroatiaItalyLithuaniaHungary

AustriaPolandSloveniaSlovakiaSweden

NorwayNorth Macedonia



For any question on data and metadata, please contact: EUROPEAN STATISTICAL DATA SUPPORT
Download

1. Contact	Top
1.1. Contact organisation
Eurostat, the Statistical Office of the European Union

1.2. Contact organisation unit
E3: Transport

1.5. Contact mail address
5, Rue Alphonse Weicker L- 2920 Luxembourg LUXEMBOURG G-D


2. Metadata update	Top
2.1. Metadata last certified	09/01/2019
2.2. Metadata last posted	09/01/2019
2.3. Metadata last update	25/09/2019

3. Statistical presentation	Top
3.1. Data description
The Air transport domain contains national and international intra and extra-EU data. This provides air transport data for passengers (in number of passengers) and for freight and mail (in 1 000 tonnes) as well as air traffic data by airports, airlines and aircraft. Data are transmitted to Eurostat by the Member States of the European Union, Iceland, Norway and Switzerland as well as by Candidate and Potential Candidate Countries (on voluntary basis). The statistics are compiled followinng the provisions of the Regulation (EC) N°1358/2003, implementing Regulation N°437/2003 of the European Parliament and of the Council on statistical returns in respect of the carriage of passengers, freight and mail by air. They are and transmitted to Eurostat on a monthly and annual basis (depending on the dataset and airport category). The air transport data are collected at airport level. As from 2003 reference year the data are provided according to the legal act (some countries were given derogation until 2005). Until 2002 partial information (passenger transport only) are available for some countries and airports.

Airports handling less than 15 000 passenger units annually are excluded from the scope of the Regulation. Datasets A1 and B1 are provided on monthly basis, while dataset C1 can be provided either on monthly or annual basis. For some countries optional variable - total number of transfer passengers - is provided as well.

The data are disseminated by Eurostat in on-line database in four sub-domains:

Air Transport measurement - Passengers
Air Transport measurement - Freight and mail
Air Transport measurement - Traffic data by airports, aircraft and airlines
Air Transport measurement - Data aggregated at standard regional levels (NUTS).
The two first domains contain several data collections:

Overview of the air transport by country and airport,
National air transport by country and airport,
International intra-EU air transport by country and airport,
International extra-EU air transport by country and airport,
Detailed air transport by reporting country and routes.
In the tables of the sub-domain "Transport measurement - Passengers", data are broken down by passengers on board (arrivals, departures and total), passengers carried (arrivals, departures and total) and passenger commercial air flights (arrival, departures and total). Additionally, the tables of collection "Detailed air transport by reporting country and routes" provide data on seats available (arrival, departures and total). The data is presented at monthly, quarterly and annual level.

In the tables of the sub-domain "Transport measurement - Freight and mail", data are broken down by freight and mail on board (arrival, departures and total), freight and mail loaded/unloaded (loaded, unloaded and total) and all-freight and mail commercial air flights (arrival, departures and total). The data is presented at monthly, quarterly and annual level.

In the tables of the sub-domain "Transport measurement - Traffic by airports, aircraft and airlines":

- Data by type of aircraft are broken down by total passengers on board, total freight and mail on board in tonnes, total passengers seats available, total commercial air flights (passengers + all-freight and mail), passenger commercial air flights, all-freight and mail commercial air flights. The data is presented at annual level since 2003.

- Data by type of airline are broken down by total passengers on board, total passengers carried, total freight and mail on board, total freight and mail loaded/unloaded, total passengers seats available, total commercial air flights (passengers + all-freight and mail), passenger commercial air flights, all-freight and mail commercial air flights. The data is presented at annual level since 2003.

- Data by airport are  broken down by total passengers carried, total transit passengers, total transfer passengers, total freight and mail loaded/unloaded, total commercial aircraft movements, total aircrafts movements. the data is presented at monthly, quarterly and annual level.

The sub-domain "Transport measurement - Data aggregated at standard regional levels (NUTS)", contains two tables:

Air transport of passengers at regional level
Air transport of freight at regional level
The tables present the evolution of the number of passengers carried (if not available passengers on board) and the volume of freight and mail loaded or unloaded (if not available freight and mail on board) to/from the NUTS regions (level 2, 1 and 0) since 1999. The data is presented at annual level. The air transport regional data have been calculated using data collected at the airport level in the frame of the regulatory data collection on air transport. More information can be found in Regional transport statistics metadata file.

For more details on datasets, data validation and issemination refer also to  Reference Manual on Air Transport Statistics available in the Annex part of the metadata.

3.2. Classification system
Airports are classified according to ICAO (International Civil Aviation Organization) airport coded as listed in ICAO document 7910.

Aircrafts are classified according to aggregated aircraft categories based on the ICAO aircraft codes as listed in ICAO document 8643.

Airlines are classified according to the ICAO airline codes as listed in the ICAO document 8585. When providing the data to Eurostat, the region where they are licensed is coded accordingly either as European Union (EU) or outside the European Union (non-EU).

3.3. Coverage - sector
Air transport - commercial air services and civil aircraft movements for the airports with traffic in excess of 15 000 passenger units annually.

3.4. Statistical concepts and definitions
Regulation (EC) N°1358/2003, implementing Regulation N°437/2003 of the European Parliament and of the Council on statistical returns in respect of the carriage of passengers, freight and mail by air, mentions three datasets: the Flight Stage dataset, called A, the On Flight Origin/Destination dataset, called B and the Airport dataset, called C.

Dataset A: This dataset contains periodic flight stage data registered for airport-to-airport routes, and broken down by arrivals/departures, scheduled/non-scheduled, passenger service/all-freight and mail service, airline information and aircraft type. The values provided concern passengers on board, freight and mail on board, commercial air flights as well as passenger seats available.

Dataset B: This dataset contains periodic on flight origin/destination data registered for airport-to-airport routes, and broken down by arrivals/departures, scheduled/non scheduled, passenger service/all-freight and mail service and airline information. The values provided concern passengers carried and freight and mail loaded or unloaded.

Dataset C: This dataset contains periodic airport data registered for declaring airports, and broken down by airline information. The values provided concern total passengers carried, total direct transit passengers, total transfer (indirect transit) passengers (optional variable), total freight and mail loaded or unloaded, total commercial aircraft movements and total aircraft movements.

Before the adoption of the Regulations, not all the participating countries were providing data (on passenger transport only) according to the two reporting concepts covered by the Regulation: "On flight origin and destination" (OFOD) and "Flight stage" (FS).

The main concepts used in this domain are the following:

Community airport

A defined area on land or water in a Member State subject to the provisions of the treaty, which is intended to be used either wholly or in part for the arrival, departure and surface movement of aircraft and open for commercial air services.

Flight stage

The operation of an aircraft from take-off to its next landing. This is linked to the definition of passengers (or freight and mail) on board.

Passengers on board

All passengers on board of the aircraft upon landing at the reporting airport or at taking off from the reporting airport. All revenue and non revenue passengers on board an aircraft during a flight stage. Includes direct transit passengers (counted at arrivals and departures).

Direct transit passengers

Passengers who, after a short stop, continue their journey on the same aircraft on a flight having the same flight number as the flight on which they arrive.

Transfer of indirect transit passengers

Passengers arriving and departing on a different aircraft within 24 hours, or on the same aircraft bearing different flight numbers. They are counted twice: once upon arrival and once on departure.

Freight and mail on board

All freight and mail on board of the aircraft upon landing at the reporting airport or at taking off from the reporting airport. All freight and mail on board an aircraft during a flight stage. Includes direct transit freight and mail (counted at arrivals and departures). Includes express services and diplomatic bags. Excludes passenger baggage.

On flight origin and destination

Traffic on a commercial air service identified by a unique flight number subdivided by airport pairs in accordance with point of embarkation and point of disembarkation on that flight. For passengers, freight or mail where the airport of embarkation is not known, the aircraft origin should be deemed to be the point of embarkation; similarly, if the airport of disembarkation is not known, the aircraft destination should be deemed to be the point of disembarkation. This is linked to the definition of passengers carried and freight and mail loaded or unloaded.

Passengers carried

All passengers on a particular flight (with one flight number) counted once only and not repeatedly on each individual stage of that flight. All revenue and non-revenue passengers whose journey begins or terminates at the reporting airport and transfer passengers joining or leaving the flight at the reporting airport. Excludes direct transit passengers.

Freight and mail loaded or unloaded

All freight and mail loaded onto or unloaded from an aircraft. Includes express services and diplomatic bags. Excludes passenger baggage. Excludes direct transit freight and mail.

Conceptually speaking, figures obtained by applying the Flight Stage concept and the On Flight Origin/Destination concept are not comparable because of direct transit passengers, which are counted for "flight stages" but not for "on flight origin/destination". The following example shows the difference between the "On flight origin and destination" data and the "Flight Stage" data: a flight is operated on a route New York-London-Paris 185 passengers travel from New York to London, 135 from New York to Paris and 75 from London to Paris. Thus in terms of on flight origin/destination data the figures recorded are 185 passengers New York-London, 135 passengers New York-Paris and 75 passengers London-Paris. New York would record the figures for New York-London and New York-Paris; London would record New York-London and London-Paris; Paris would record New York-Paris and London-Paris. In terms of flight stage data there are two flight stages and the figures reported by New York and London airports are: New York-London 320=(185+135) passengers and by London and Paris airports are London-Paris 210=(135+75) passengers.

3.5. Statistical unit
The data used in the domain are collected from different data providers (mostly from airports or/and airlines depending on a country and dataset) at the airport level.

3.6. Statistical population
Four categories of Community airports (together with their reporting obligations) are defined by the Regulation (EC) N°1358/2003:

Category "0": Airports with less than 15 000 passenger units per year are considered as having only "occasional commercial traffic" without obligation to report. However, some countries report data on these airports which are disseminated.

Category "1": Airports with between 15 000 and 150 000 passenger units per year shall transmit only aggregated airport data (Dataset C).

Category "2": Airports with more than 150 000 passenger units and less than 1 500 000 passenger units per year shall transmit flight stage data, on flight origin destination data as well as aggregated airport data (Datasets A, B and C).

Category "3": Airports with at least 1 500 000 passenger units per year shall transmit flight stage data, on flight origin destination data as well as aggregated airport data (Datasets A, B and C).

Please note airport categories 2 and 3 were created for the Regulation's implementation period (for being granted derogations in data reporting, for example) and in order to distinguish the biggest airports (hubs). The two categories have the same level of reporting obligations. In general airports of category ‘0’ are not included in the statistics provided to Eurostat (nevertheless a country might include statistics for such airports on a voluntary basis).

List of reporting airports by country with categories as from 2003 onwards as well as the list of reporting airports for the recent year is available in the Annex part of the metadata.

3.7. Reference area
The data collection covers national and international airports and aerodromes with commercial air transport and non-commercial general aviation activities exceeding 15 000 passenger units per year and which are located on the territory of the EU Member States, Norway, Iceland, Switzerland, as well as the Candidate Countries and Potential Candidate Countries (which may provide the data on voluntary basis).

3.8. Coverage - Time
Air transport statistics based on the Regulation 437/2003 are disseminated starting from the reference year 2003 (for some countries 2002 data are available, for the others countries which were given derogations - form 2004 or 2005). Depending on a country, partial information are available starting from 1993 reference year. Until 1996, all participating countries declared yearly data. However, for some of them, quarterly data appeared from 1997 onwards and monthly from 1998 onwards.

3.9. Base period
Not applicable


4. Unit of measure	Top
The units used depend on the variables collected within each dataset and are: number of passengers, tonnes or kilograms (for freight and mail), flights, aircraft movements and seats available.


5. Reference Period	Top
Data is collected on a monthly basis and then aggregated at quarterly and annual level. The latest reference period available is 2018.


6. Institutional Mandate	Top
6.1. Institutional Mandate - legal acts and other agreements
This data transmission is based on a EP and Council framework legal act and on several implementing Commission Regulations:

Regulation (EC) No 437/2003 of the European Parliament and of the Council of 27 February 2003 on statistical returns in respect of the carriage of passengers, freight and mail by air.
Commission Regulation (EC) No 1358/2003 of 31/07/2003.
Commission Regulation No 546/2005 of 8 April 2005.
Commission Regulation No 158/2007 of 16 February 2007.
Before 2003, the data was provided by the different participating countries on a voluntary basis.

6.2. Institutional Mandate - data sharing
As concerns air transport statistics. there are no specific agreements related to data sharing and exchange between Eurostat and national or international data collecting/producing agencies/bodies. Eurostat collects the statistics based on the legal act (Regulation 437/2003). All users have equal access to statistical releases at the same time.


7. Confidentiality	Top
7.1. Confidentiality - policy
Regulation (EC) No 223/2009 on European statistics (recital 24 and Article 20(4)) of 11 March 2009 (OJ L 87, p. 164), stipulates the need to establish common principles and guidelines ensuring the confidentiality of data used for the production of European statistics and the access to those confidential data with due account for technical developments and the requirements of users in a democratic society.

As concerns air transport statistics, the data provided to Eurostat in the datasets A1, B1 and C1 contain no confidential information. There are some restrictions put on the dissemination of detailed information on airlines, which are sometimes considered as sensitive (especially when combined with other variables, like routes or/and number of passengers transported). That's why the data providers may deliver this information with a higher level of aggregation (coded into EU and non-EU). Eurostat anyway disseminates airline information  with EU or non-EU registered airline labels only.

There are also restrictions for dissemination of some detailed information on routes. As agreed with the data providers, only routes above a certain threshold are disseminated. The rules for selection of the routes between the 'main declaring airports' and their 'main partners' are described in details in Annex XV of the Reference manual on air transport statistics (available in the Annex part of the metadata).

7.2. Confidentiality - data treatment
There are specific extractions procedures, which do not allow the data with dissemination restrictions (airline information and routes under specific threshold) to be included in the files which feed on-line tables (Eurobase).


8. Release policy	Top
8.1. Release calendar
The majority of the tables in the on-line database is updated once per quarter. Some tables, coming mostly from a voluntary data collection (fleet, airport infrastructure), air transport accident and regional tables are updated only once a year.

There is no specific release calendar for any of the data, as the release date depends on the data provisions from all reporting countries. The release date might then differs slightly from year to year.

There is news release publication summarizing passenger transport by air in the EU countries is prepared each year and released begininng of December.

8.2. Release calendar access
There is not a precise calendar of updates apart from the periodicity mentioned abov.

8.3. Release policy - user access
In line with the Community legal framework and the European Statistics Code of Practice Eurostat disseminates European statistics on Eurostat's website (see item 10 - 'Accessibility and clarity') respecting professional independence and in an objective, professional and transparent manner in which all users are treated equitably. The detailed arrangements are governed by the Eurostat protocol on impartial access to Eurostat data for users.

Following mentioned above rules all users have equal access to statistical releases on air transport at the same time.


9. Frequency of dissemination	Top
The majority of the tables (derived from the data collected within the legal act) in the on-line database is updated once per quarter. Some tables (presenting annual data), coming mostly from a voluntary data collection (fleet, airport infrastructure), air transport accident and regional tables are updated only once a year.


10. Accessibility and clarity	Top
10.1. Dissemination format - News release
There is news release publication - on-line version - summarizing passenger transport by air in the EU countries is prepared each year and released begininng of December.

10.2. Dissemination format - Publications
Statistics Explained article Air passenger transport - monthly statistics  (three updates per year)

Statistics Explained article - Air transport statistics - annual data (one updated per year)

Eurostat Pocketbook Energy, transport and environmental indicators - 2018 edition

Eurostat Regional Yearbook - 2018 edition, (Statistics Explained edition, Statistics Explained 2018 edition), previous editions.

Passenger transport by air - visualisation tool.

10.3. Dissemination format - online database
Please consult free data on-line (Transport/Air transport). Data availability tables file is prepared with every major Eurobase data update (available in the Annex part of the metadata).

10.4. Dissemination format - microdata access
Not applicable for air transport statistics.

10.5. Dissemination format - other
Not applicable for air transport statistics.

10.6. Documentation on methodology
The Reference Manual on Air Transport Statistics - contains detailed methodological information as well as background information on the implementation of the legal acts and on how data are processed and disseminated by Eurostat - avialable in the Annex part of the metadata.

Additional definitions of the terms used in the frame of the statistics on air transport are available in the "Glossary on Air Transport Statistics" - avialable in the Annex part of the metadata.

Country Specific Notes (CSNs) - avialable in the Annex part of the metadata.

Methodological notes are also available in the different publications on Aviation statistics (see point 10.2).

10.7. Quality management - documentation
The Reference Manual (Annex XIII - Quality summary results) includes a section describing the quality checks applied to the incoming data as well as showing the results of the quality analysis of the data received - avialable in the Annex part of the metadata.


11. Quality management	Top
11.1. Quality assurance
The system on statistics on air transport follows as far as possible the European Statistics Code of practice and Quality Assurance Framework of the European Statistical System, more precisely Principle 4: Commitment to Quality.

Data quality is ensured by the implementation of a common and well established methodology (Reference manual on air transport statistics - available in the Annex part of the metadata) for the data collection and compilation at country level. Data is subsequently validated in Eurostat by applying different controls (codification, format checks, consistency over time, inter-datasets checks) on every incoming data set, before and after treatment, as well as by cross-checking partner countries figures (mirror statistics). The results of data quality control are always provided to the reporting country either for information or for comments or/and corrections.

11.2. Quality management - assessment
Data quality is monitored on regular basis and in general can be assessed as high, but might differ a little bit from country to country. There is a set of validation rules and quality checks put in place, which detects various types of issues. The results of the validation process run on every single dataset are always communicated to the data providers. In case of any issues detected, each data provider needs to provide explanations or/and revise the data accordingly. Annually, a data quality report is prepared with a summary of the main findings affecting quality as well as showing the solution adopted and the materiality of the existing differences. It is provided to each country in order either to correct the existing data or to improve the data quality for the future data transmissions. Moreover mirror checks are prepared and provided to all reporting countries in order to improve consistency.

Moreover, data quality aspects are always on the agenda of the bi-annual Working Group on Air transport Statsitics.


12. Relevance	Top
12.1. Relevance - User Needs
The usual users of air transport data are people from different Commission Services or other European institutions (e.g.: DG MOVE, DG REGIO, DG COMP, the European Court of Auditors), National Statistical Authorities, international or other governmental institutions (Ministries of Transport), universities or research institutions, journalists as well as the users involved in the industry as airlines, airports or air traffic management.

Users mainly request these data to properly monitor the development of air transport in the EU and other European countries, evaluate the impact of the air transport industry in the economy, quantify the importance of the transport flows of passengers and freight at intra-EU and extra-EU level and assess the competition in the air transport market.

12.2. Relevance - User Satisfaction
The existing data collection on air transport statistics is well appreciated by the users are cover major users' needs. Accuracy, clarity and comparability are particularly indicated as good qualities of these data. European air transport statistics are a valuable resource to a wide range of users.

Timeliness and lack of information on major partner countries (for extra-EU transport) are cited as weaknesses of this domain.

For more details, please refer to the latest Rolling Review of 2009 (Annex part of the metadata).

The users' needs expressed recently concerns data on true origin and destination, which is not covered by the legal act.

12.3. Completeness
Completeness of data is high. There is an obligation of data provision for the Member States and EFTA countries, as a consequence, there are very few gaps in the data provision, at least since 2003 when the framework legal act came into force.

The information available in table avia_af_apal on transfer (indirect transit) passengers do not cover all reporting countries and airports, because of voluntary nature of transfer passengers data provisions introduced as from 2010 year onwards.

The completenes of the data provided by the Candidate and Potential Candidate Countries differs from country to country, as they provide air transport statistics on voluntary basis.


13. Accuracy	Top
13.1. Accuracy - overall
Overall accuracy of the data is good. Regular mirror checks excercises and comparisons with other relevant international sources (ICAO, airports and airlines data) shows a high level of comparability.

13.2. Sampling error
Not applicable for air transport data collection.

13.3. Non-sampling error
Not applicable for air transport data collection.


14. Timeliness and punctuality	Top
14.1. Timeliness
According to the existing legal basis, countries have 6 months to deliver the data for the reference monthly period. Some countries experience for time to time problems in providing the data on time (delayed data compilation by airports, IT issues, additional checks and clarifications with the data providers needed, etc.). Eurostat needs another 2-3 months to process (additional quality checks are done) and disseminate final data together with EU aggregates (as for that the final data from all EU countries are needed). It means that the final annual data are disseminated about nine months after the reference period, usually together with Statistics Explained articles. Monthly and quarterly provisional data are disseminated earlier, depending on data deliveries from the reporting countries.

14.2. Punctuality
A majority of countries deliver the data earlier than the t+6 months regulatory deadline. The rest of the countries respect the deadline for data provision with some occasional exceptions. Usually the monthly and quarterly statistics are disseminated within two months after receiving the datasets. Final annual data together with EU-aggregates are disseminated about nine months after the reference period.


15. Coherence and comparability	Top
15.1. Comparability - geographical
Data comparability across countries is very high. This is ensured by the implementation of a common methodology. In addition, the so-called "mirror checks" allow to compare the data declared by partner reporting airports and find possible inconsistencies that are corrected as far as possible.

15.2. Comparability - over time
As from 2002 the statistics on air transport are comparable over time, as they are collected following fully the provisions of the legal act - the Regulation 437/2003. The series breaks occur in case of airport being closed temporarily (for maintenance, reconstructions, etc.) or in case an airport does not exceed the threshold of 15 000 passenger unit per year (and may be excluded from air transport data provisions) or an airport which appears in the reporting because of reaching the reporting threshold. The detailed information on eventual breaks in the time series is provided in the Country Specific Notes (CSNs) available in the Annex part of the metadata.

The data collected before 2002 (before the leagl act) might not be fully coherent with the ones collected nowadays (depends on a country and time period concened).

The figures disseminated in all avia_gor_ and avia_par tables (Detailed air passenger transport by reporting country and routes, Detailed freight and mail air transport by reporting country and routes) are compiled from detailed statistics provided to Eurostat by the reporting countries. In order to select the routes for dissemination, thresholds were defined separately for passenger and freight transport, because the importance of a route may be different in terms of passenger transport and in terms of freight and mail transport.

It was agreed with the reporting countries that only those routes which are above the thresholds are disseminated in Eurobase tables. The figures which are not disseminated (because the threshold is not reached) are marked with ':'. Please note the ':' might mean either data not disseminated because the threshold was not reached or there was no transport on this route. The methodology on how the routes between the main declaring and the main partner airports are selected is available in Annex XV of the Reference Manual (pp. 275-279) published on the Eurostat website.

15.3. Coherence - cross domain
The statistics collected from EU and EFTA countries are fully in line with the provisions of the Regulation 437/2003. Eurostat publishes the information collected in the agreed (with the reporting countries) set of disssemination tables (Eurobase tables). That's why the data coherence across the domain is good. Detailed data at airport level are available: national, international intra- and extra-EU figures at the EU, country, and airport levels. Data at regional level (NUTS level 2, 1 and 0) are also available. they shows good coherence as they are derived from detailed statistics at airport level.

15.4. Coherence - internal
The statistics collected from EU and EFTA countries are fully in line with the provisions of the Regulation 437/2003. The additional data structure, completenes and consistency checks ensure high level of data quality and consistency within each dataset.


16. Cost and Burden	Top
An exercise to measure cost-benefit of this obligatory data collection was made in 2007. However, the methodology applied to compare both sides did not allow to provide consistent and valuable conclusions, particularly because of the difficulty of comparing costs (measured in a currency) and benefits (measured with a scoring system).


17. Data revision	Top
17.1. Data revision - policy
Eurostats takes into account any revisions of the data already provided by the reporting countries and disseminated in Eurostat's on-line database.

17.2. Data revision - practice
Every incoming data set is subsequently validated in Eurostat by applying different controls - codification, format checks, consistency over time, inter-datasets checks). In case of issues detected or doubts, Eurostat comes back to the reporting country asking for clarifications or/and revisions in case of any issues detected or doubts. Only data which passes through all controls are considered as final and are put into dissemination. In case of any further revisions provided by a reporting country, they are always taken into account and disseminated, replacing the previous ones. In case of substantial revisions a note is prepared and available in the Country Specific Notes (CSNs) usually providing the reason for the data revisions. The dedicated flag 'r' (indicating revised figures) is not applied in the dissemination of air transport data.


18. Statistical processing	Top
18.1. Source data
Eurostata collects air transport statistics from national authorities, which can be for instance the Statistical Office, Ministry of Transport or the Civil Aviation Authority. It depends on the organisation of the data collection arranged in each reporting country. Original data sources for national authorities are normally the airports organisations or enterprises as well as airlines.

18.2. Frequency of data collection
Air transport statistics are collected on a monthly basis as regards datasets A1 and B1 which cover airports of categories 2 and 3 (with annual traffic of more than 150 000 passenger units). Dataset C1 can be delivered on a monthly, quarterly or annually basis (as this dataset covers all airports with annual traffic of more than 15 000 passenger units).

18.3. Data collection
Data should be transmitted to Eurostat using the EDAMIS engine tool following the transmission format foreseen in the Regulation 1358/2003.

18.4. Data validation
Data validation takes place at several levels of the data processing. The first step is the automatic validation during the integration, and the second step is the quality checks when data has been integrated.

Automatic validation

The checks especially ensure that:

 the record format is correct
 there are no duplicate records within the dataset
 each record contains valid and correct codes (specific focus is put on airports, airlines and aircarft types codes, for which the ICAO codes have to be used)
Quality checks

Three types of quality checks are made on the datasets received for national and international transport.

Consistency over time - this check is made in order to detect unlikely increase or decrease of transport at one of the reporting airports. This check is applied separately for international and national transport.
Mirror checking - these quality checks are performed in order to compare the consistency between two partner declarations. They are run both for national and international declarations at city level. This means that the reported data have been first aggregated at city levels and then compared. This allows detecting and solving potential problems of wrong airport code attribution.
Missing routes - this check allows detecting the routes between two declaring airports where only one of them has declared the information. It is run for international and national transport separately.
18.5. Data compilation
In the frame of the data dissemination process, Eurostat has to calculate aggregates at intra-EU level (national, regional and intra-EU aggregates). It requires sometimes solving the problem of double counting. For each aggregate it is necessary to start at the airport level in order to identify the mirror declarations, i.e. the airport routes for which both airports report the volume, since these constitute the routes where the problem of double counting occurs. When calculating the total volume in such cases, only the departure declarations of the concerned airports have been taken into account. The problem of the double counting only appears for the calculation of the total passengers but not for the total arrivals (respectively total departures), which corresponds to the sum of the arrivals (respectively departures) at each domestic airport.

Please note, that because of mentioned above double counting issue, share of an airport in the EU-totals (for passenger or freight) should be calculated by summing up all countries' totals first (and not by taking the EU totals available in the tables) and only then compared with airport total transport.

Concerning the total international extra-EU transport, the calculation is easier. It consists in the sum of all the declarations of the Member States to/from all the partner countries out of the European Union, as there is no double counting.

18.6. Adjustment
Not applicable for air transport data collection.


19. Comment	Top
In avia_tf_apal dissemination table the figures on passenger carried include transfer (indirect transit) passenger figures (but not direct transit passengers).

Refer to Country Specific Notes (CSNs) (available in the Annex part of the metadata) for dedicated methodological notes on the data provided by each reporting country (break in the time series, changes in the reporting patterns, explanation on important data fluctuations or data revisions, etc.).


Related metadata	Top

Annexes	Top
Glossary on air transport statistics
Rolling Review - Air Transport Statistics - Final Report -11 September 2009
Reference Manual on Air Transport Statistics version 14
Data availability tables as of July 2018
Country Specific Notes (CSNs) version of 07 2018
Airport categories - time series
List of reporting airports for 2018 reference year covered by Commission Regulation 1358/2003
