Title: Rail + Fly
Date: 2020-02-02
Category: nighttrain
Tags: night train, flying
Slug: filter-data

Here, I will filter down the {% include 'vars/all_links' %} flight connections
that are downloaded from the EU database. But those also contain flight connections to other
continents that are not relevant here. Those are split up into

* all inter-european flights
* all flights within the European Economic Area ({% include 'vars/text/countries_eea' %})
* all flights within the core area ({% include 'vars/text/countries_core' %})

These geographic splits are then further filtered down. 
The data is in the format `#people transported on flight connections` (`#flight connections`).

{% include 'tables/filter.html' %}

# Coverage 

Depending on how long night trains are acceptable (above we said {%include 'vars/max_duration'%}),
a different percentage of flight connections / air passengers can be replaced with trains. 

## Flight Connections within Europe

### With an existing rail connection (`valid`)
![coverage](../images/covered.png)

### Disregard Rail network gauge (`all_gauge`)
![coverage all gauges](../images/covered_all_gauge.png)

## Train connection in standard gauge in the EEA 

![coverage EEA](../images/covered_eea.png)

## Train connections in standard gauge in the core European Area

![coverage Core](../images/covered_core.png)
