Title: The Source Data
Date: 2020-01-24
Category: nighttrain
Tags: night train, flying
Slug: fly-data

The question of this project is to find out what flight routes within Europe could actually be 
replaced by trains. For this high-speed day trains as well as ultra long distance night trains 
are being considered. 

The [Passenger Data](../data_air/explanation.txt) that is the foundation of this analysis 
is provided by the EU. the passenger data of all medium and large-sized airports within the EU
to their main destinations are provided. The data contains: 
- number of passengers transported
- number of seats offered
- number of trips made (really?)

For some airports the data is only provided per quarter while others provide 
data on a monthly basis. While there is data for more than 20 years ago, 
many connections only provide data for the last quarter(s). 

For simplicity, in the following only the data for 2019-Q2 is considered 
as for many airports at the creation of this report no newer data was available.
When not even this quarter was available, the data from a previous quarter was taken.

Furthermore, all routes that don't lie between 35N 11W and 65N 39E are being 
ignored, as we can only find train connections within continental Europe. 

This map shows the main airport with more than 3000 passengers transported in 2019-Q2:

![Relevant Airports](../images/airports_codes.png)
