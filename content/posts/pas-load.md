Title: Passenger Load on Links
Date: 2020-02-01
Slug: pas-load

In this post, I want to analyze, what passenger load 
would be on links of the network, if all flights 
(up to a certain duration) were operated as train lines. 

{% for i in range(100, 2100, 100) %}

![Passenger Load for routes below {{i}} min](images/pas/pas{{ i }}.png)
    
{% endfor %}