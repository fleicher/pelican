Title: Änderungsvorschläge für SEM München Nord-Ost
Date: 2020-10-08
Slug: sem

# Optimierung der Verkehrssituation für das Neubaugebiet München Nord-Ost

Im Frühjahr 2020 hat München einen Wettbewerb ausgeschrieben,
das geplante Neubaugebiet im Nordosten der Stadt zu entwerfen. 
Den ersten Preis gewann der Entwurf von 
**rheinflügel severin** (Düsseldorf) und **bbz landschaftsarchitekten berlin gmbh bdla** (Berlin).
[Quelle](https://www.muenchen.de/rathaus/Stadtverwaltung/Referat-fuer-Stadtplanung-und-Bauordnung/Stadt-und-Bebauungsplanung/Wettbewerbe/Nordosten.html)

Auch mir gefällt dieser Vorschlag ziemlich gut, da er sehr viele Grünzüge im Gebiet ermöglicht, den Badesee optimal an den neuen Stadtteil anbaut und die bestehenden Orte gut einbindet. Auch sehe ich es als sehr sinnvoll an, die U4 nach München Riem (und Messe West) zu verlängern da hier gutes Fahrgastpotential besteht. Natürlich sollte hier die U-Bahn gebaut werden bevor die darüber liegende Straße fertiggestellt wird um Kosten zu sparen. Ebenfalls finde ich es sehr sinnvoll, eine Straßenbahn in Nord-Süd Richtung zu planen. 

## Vorschläge

![Vorschläge](../images/Screenshot_2021-02-20_at_15.29.20.png)

Ich will hier ein paar Vorschläge machen, um die Verkehrssituation zu verbessern. Die folgende Karte ist eine Abänderung des [Originalplan](https://www.muenchen.de/rathaus/dam/jcr:f0d00fdc-01e7-425f-a4d6-28f67331fe51/Plan_30.000_rheinfluegel.jpg) des Gewinnervorschlags für 30.000 Einwohner.
* **A**: Im Plan ist vorgesehen, den Verkehr von München (von Westen her) als auch dem Kerngebiet des SEM (von Süden her) ins Gewerbegebiet nach Norden über die vorhandene Straße Wacholderweg in Johanneskirchen zu leiten. Im Norden des Gewerbegebiets ist ebenfalls eine Anbindung an die Region über die Straße M3 geplant. Der Wacholderweg ist zu eng, um diesen Bedürfnissen gerecht zu werden. Der breite Querschnitt der westlichen Johanneskirchner Straße sollte diagonal nach Nordosten weitergeführt werden und in die nördliche Anbindungstraße übergehen. So könnte auch, sollte sich der Bedarf ergeben, die Metrobuslinie 50 (oder eine Tram) über die S-Bahn-Station Johanneskirchen hinaus verlängert werden und auf separaten Busspuren ins Gewerbegebiet Unterföhring geführt werden. Der Wacholderweg könnte für Anwohner beschränkt werden. 

* **B**: Zurzeit wird die Gartenstadt Johanneskirchen über den Stadtbus 154 (mit 10 Minuten Takt zur Hauptverkehrszeit) angeschlossen. Aktuell endet der Bus in der Zahnbrecher Siedlung. Im Rahmen des Neubaugebietes, wäre eine (weitere) Verbindung von der Gartenstadt Johanneskirchen nach Süden sinnvoll. Um den Fahrweg abzukürzen, könnte eine Öffnung des nördlichen Endes der Johannes-Neuhäuser-Straße zumindest für den ÖPNV sinnvoll sein.

* **C**: Aktuell ist eine Tramlinie zur Erschließung des Neubaugebiets angedacht. Langfristig wäre aber auch eine Tramverbindung in das Gewerbegebiet Trudering am Schatzbogen möglich. Deshalb sollte beim Überplanen der Trabrennbahn darauf geachtet werden, eine Trasse nach Osten freizuhalten. Die Alternativen wären eine Führung über die Rennbahnstraße oder über den südlichen Grünzug. Die erste Alternative müsste aber die Vorgärten der Anlieger mit einbeziehen und die zweite würde die Aufenthaltsqualität im Grünzug reduzieren. Durch diese zusätzliche Verbindung könnte der geplante neue S-Bahnhof an der S2 optimal angebunden werden. Die Straßenbahn könnte von dort über Riemer Straße, Schatzbogen, Halfinger Straße, und Kreillerstraße zur Haltestelle an der St.-Veit-Straße angebunden werden. 

* **D**: Im Siegerplan ist vorgesehen, den durch die Rennbahnstraße von Südosten (u.a. von der Autobahn) kommenden Erschließungsverkehr durch den Ortskern von Daglfing zu leiten. Der Querschnitt der Kunihohstraße ist wohl ausreichend, allerdings wird eine solche Belastung des historischen Ortskerns auf erheblichen Widerstand stoßen. Hier wird deshalb vorgeschlagen, die Rennbahnstraße nur indirekt über die verlängerte Daglfinger Straße an den Ortskern von Daglfing anzuschließen. Der größte Teil des Verkehrs wird daher über die neu-zu-bauende östliche Parallele fahren, welche eine ausreichende Dimension haben wird. Der Nachteil dieser Lösung ist allerdings, dass die Qualität des Grünzugs nahe der Grundschule etwas eingeschränkt wird, da sich der Abschnitt in welchem der Grünzug von einer Straße begleitet wird verlängert.

* **E**: Da München stetig wächst, ist es nicht auszuschließen, dass auch noch weitere Baugebiete im Osten erschlossen werden sollen. So gab es auch Pläne die Pferderennbahnen im Osten des Planungsgebietes zu überbauen. Dies ist hier nicht geplant. Die sinnvollste Erschließung hierzu wäre eine Verbindung der verlängerten Brodersenstraße mit dem Schatzbogen inklusive Brücke über den Hüllgraben. Zwar ist es fraglich, ob eine Zerteilung des Naturraum Hüllgraben sinnvoll ist, die Möglichkeit sollte aber zu diesem Zeitpunkt nicht „verbaut“ werden. Dementsprechend sollte der geplante Busbetriebshof im Süden nicht eine Verlängerung des Schatzbogen ausschließen. Weiterhin ließe sich die Brodersenstraße leichter verlängern, wenn sie am Rand des neu geplanten Wohngebiets östlich von Daglfing entlanggeführt wird. Im Gegenzug kann eine innere Erschließungstraße dieses Wohngebietes entfallen. In diesem Zusammenhang sei darauf hingewiesen, dass durch eine leichte Verlegung der geplanten Kleingärten ein durchgehender öffentlicher Grünstreifen von der Steiner Schule zum Hüllgraben entstehen kann. Durch diese Verlegung können eventuell auch ein paar der jetzt bereits vorhandenen Kleingärten an dieser Stelle erhalten bleiben.

## Links:

* [Stadtentwicklungsplan](https://www.ris-muenchen.de/RII/RII/DOK/SITZUNGSVORLAGE/4350564.pdf)
* [rheinflügel severin](https://www.rheinfluegelseverin.de/staedtebau/muenchner-nordosten/)
* [bbz landschaftsarchitekten](https://www.bbz.la/projekt/wb-munchen-nordosten/)
