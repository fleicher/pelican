Title: Train Data
Date: 2020-01-24
Category: nighttrain
Slug: train-data

Data Sources 
============

The objective is to calculate the possible travel time between 
any two stations of the railway network. Several websites, 
such as [Deutsche Bahn](http://bahn.de) or [Rio2Rome](http://rio2rome.com) 
offer point-to-point connection data between stations. 
But this includes waiting times at central nodes + the time for
intermediate stops. Here we consider an ideal train connection
that will use the existing network. Here, we consider ideal connections so changes 
will be neglected. As nights trains mostly run during times of low 

To calculate the travel time between railway stations, 
the following data is taken. 

* Trainline.eu's [overview](https://www.eurail.com/en/plan-your-trip/railway-map) on travel times
* Rio2Rome.com's fastest point to point connections between stations if the above was not available. 

![Trainline.eu's Travel Times](../images/trainline-times.png)


Gathering Process 
=================

We considered all cities from the Wikipedia high speed railway network map. 
![Wikipedia Railway Highspeed](../images/High_Speed_Railroad_Map_of_Europe.svg)
Each cities was mapped with a 3 letter abbreviation and the travel
times to the surrounding cities (guided by the map above) were queried
from the available data sources. The final data collected can be found 
[here](../data/rail_travel_times.json). 
Note: Times are provided as `hh.mm`, 
which cannot be interpreted as a decimal number. 


Travel Speeds
=============

There are several sources to find the travel speeds between cities. 
Here are a few 
[sources](https://www.theguardian.com/news/datablog/2009/aug/05/rail-transport-transport) 
that provide high speed lines.



This is an analysis how fast the trains run (point to point). 
Here, we just took each cities coordinates and calculated the between them. 


![Travel Speed between major Hubs in Europe](../images/speed.png)


