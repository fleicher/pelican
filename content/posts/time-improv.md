Title: Passenger Travel Time Improvements
Date: 2020-02-23
Slug: time-improv

In this post, I want to analyze, the individual benefit of 
increasing the travel time on a specific link to bee-line {%include 'vars/max_duration'%} km/h 
if it isn't already. Note that the actual average speed on high speed lines will be
considerably higher but a train can't always go straight...
Below, it is analyzed how much the passenger improvement 
could be reached by upgrading a specific link. 
The importance of improving a specific link is expressed by ???
For a specific graphic, I only consider air links, 
that could be pushed under a limit of `n` minutes. 


{% for i in range(100, 2100, 100) %}

![Passenger Travel Time improvements for routes below {{i}} min](images/improv/pas{{ i }}.png)
    
{% endfor %}

Furthermore, I have taken the data for all train rides <= {% include 'vars/max_dur_improv' %} 
and ordered the individual links between cities by their biggest ridership * time / distance gain. 
Now, one by one, I assume that this link is upgraded to a high speed one.
In the graphic below we can see, how many more connections, that previously fell above the 
limit of {% include 'vars/max_duration' %}, are now convertible to a night train connections. 

![#Night Trains by increasing High Speed Links](../images/covered_improv.png)

NOTE: in this analysis, I am not (yet) considering the fact, that by building new high speed
connections, the routes for other trains will change (which I should). If we included this, 
the rise should be much sharper.