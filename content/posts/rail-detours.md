Title: Rail + Fly
Date: 2020-01-29
Slug: rail-detours

Problems with the Rail Input Data
=================================

Since the rail travel times, as layed out [here](../posts/train-data.md),
were entered manually they are prone to errors.  


So the script calculates when for the direct connection between 
two cities there is a faster alternative that goes via
a (or multiple) other cities. This must obviously be wrong. 

Here are the top routes that are most of an detour 
with the current data. Probably there is something in need of a fix. 


All (source, target) pairs of railway station in the given 
rail network are analyzed. For each such pair, two routes can be found: 

* fastest route: the one with the lowest travel time
* shortest route: For each train connection that exists, the GPS positions 
  of the source and target cities are taken. Each connection is then associated
  with the direct connection between the two cities. 
  
For each of these routes, the following is provided:

* travel duration between the two stations
* travel distance (direct beeline distances between cities)
* intermediate stations between source and target. 

Greatest Distance Detours
=========================

{% include 'detours_distance.html' %}

Greatest Duration Detours
=========================

{% include 'detours_duration.html' %}

All Travel Durations
====================

![Travel Durations](../images/duration.png)
